import json
import allure
from tools.api.users_api import UserApi


@allure.feature("Тестирование api")
@allure.story("Тест-кейс 3: Тестирование пользователя через api.")
def test_case_user_api() -> None:
    """
    Тестовая функция тестирования пользователя через api.
    Представляет собой тест-кейс со следующими шагами:
    1. создается пользователь.
    2. логин пользователя.
    3. запрос данных пользователя.
    4. logout пользователя.
    5. удаление пользователя.
    6. запрос данных удаленного пользователя.
    :return: None.
    """

    api = UserApi()

    with allure.step("Шаг 1: Создание пользователя. "
                     "Проверяем ответ сервера."):
        assert api.create_user().status_code == 200, "create user error"

    with allure.step("Шаг 2: Логин пользователя. "
                     "Проверяем ответ сервера."):
        assert api.login_user().status_code == 200, "login user error"

    with allure.step("Шаг 3: Запрос данных пользователя."):
        with allure.step("Проверяем ответ сервера."):
            assert api.user_info().status_code == 200, "user info error"

        with allure.step("Проверяем данные пользователя."):
            assert json.loads(api.user_info().content) == \
               UserApi.CREATE_USER_BODY, "incorrect user info"

    with allure.step("Шаг 4: Logout пользователя. Проверяем ответ сервера."):
        assert api.logout_user().status_code == 200, "user logout error"

    with allure.step("Шаг 5: Удаление пользователя. Проверяем ответ сервера."):
        assert api.delete_user().status_code == 200, "delete user error"

    with allure.step("Шаг 6: Запрос данных удаленного пользователя. "
                     "Проверяем ответ сервера."):
        assert api.user_info().status_code == 404, "user exists"
