import psycopg2
import pytest
from selenium import webdriver
# from selenium.webdriver import Safari


@pytest.fixture(scope="session")
def driver():
    """
    Фикстура страта и закрытия драйвера.
    :return: драйвер.
    """
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    driver = webdriver.Chrome(chrome_options=chrome_options)
    driver.maximize_window()
    driver.implicitly_wait(15)
    # driver = Safari()
    yield driver
    driver.quit()


@pytest.fixture(scope="session")
def database():
    """
    Фикстура подключения к БД.
    Создает подключение к БД.
    :return: db.
    """

    db = psycopg2.connect(user='postgres',
                          password='postgres',
                          host='host.docker.internal',
                          database='postgres')
    return db


@pytest.fixture(scope="session")
def cursor(database):
    """
    Фикстура работы с БД.
    Создает курсор и закрывает подключение к БД.
    :return: cursor.
    """

    cursor = database.cursor()
    yield cursor
    cursor.close()
