import allure
from tools.pages.main_page import MainPage
from tools.sql.main_sql import MainSql


@allure.feature("Тестирование sql и web")
@allure.story("Тест-кейс 1: Создание группы через sql и проверка ее через web")
def test_case_add_new_group(driver, database, cursor):
    """
    Тестовая функция тестирования создания группы через sql и проверка группы
    через web.
    Представляет собой тест-кейс со следующими шагами:
    1. Создание группы через sql.
    2. Открытие окна главной страницы.
    3. Вход в админку.
    4. Переход на страницу groups.
    5. Поиск группы в списке групп.
    :param driver: драйвер.
    :param database: db.
    :param cursor: cursor.
    :return: None.
    """

    db = database
    group_name = MainSql.GROUP_NAME

    groups = MainSql(cursor, db)
    groups.add_new_group()
    new_group = groups.get_group_name()

    with allure.step("Шаг 1: Создание группы. Проверяем группу в БД"):
        assert group_name == new_group[0][0], "incorrect new group name"

    main_page = MainPage(driver)
    main_page.open()

    with allure.step("Шаг 2. Переход на главную страницу"):
        with allure.step("Проверяем title"):
            assert main_page.get_title() == "Hello, world!", \
                "incorrect main page title"

        with allure.step("Проверяем url"):
            assert main_page.get_url() == "http://host.docker.internal:8000", \
                "incorrect main page url"

    login_admin_page = main_page.click_admin_button()
    login_admin_page.input_username_field()
    login_admin_page.input_password_field()
    admin_page = login_admin_page.click_login_button()

    with allure.step("Шаг 3: Вход в админку"):
        with allure.step("Проверяем title"):
            assert admin_page.get_title() == \
                   "Site administration | Django site admin", \
                   "incorrect admin page title"

        with allure.step("Проверяем url"):
            assert admin_page.get_url() == \
                   "http://host.docker.internal:8000/admin/", \
                   "incorrect admin page url"

    groups_page = admin_page.groups_page_click()
    url = "http://host.docker.internal:8000/admin/auth/group/"

    with allure.step("Шаг 4: Переход на страницу groups"):
        with allure.step("Проверяем title"):
            assert groups_page.get_title() == \
               "Select group to change | Django site admin", \
               "incorrect groups page title"

        with allure.step("Проверяем url"):
            assert groups_page.get_url() == url, "incorrect groups page url"

    group = groups_page.get_group(group_name)

    with allure.step("Шаг 5: Поиск группы в списке. "
                     "Проверяем что новая группа есть в списке"):
        assert group.text == group_name, "group absent"

    admin_page.logout()


@allure.feature("Тестирование sql и web")
@allure.story("Тест-кейс 2: Создание пользователя через web и "
              "проверка через sql")
def test_case_add_new_user_in_group(driver, database, cursor):
    """
    Тестовая функция тестирования создания пользователя и
    добавления его в группу через web и проверка что пользователь в группе
    через sql.
    Представляет собой тест-кейс со следующими шагами:
    1. Переход на строницу создания пользователя.
    2. Создание пользователя.
    3. Добавление пользователя в группу и проверка через sql.
    4. Выход из админки.
    :param driver: драйвер.
    :param database: db.
    :param cursor: cursor.
    :return: None.
    """

    main_page = MainPage(driver)
    main_page.open()
    login_admin_page = main_page.click_admin_button()
    login_admin_page.input_username_field()
    login_admin_page.input_password_field()
    admin_page = login_admin_page.click_login_button()
    users_page = admin_page.users_page_click()
    add_user_page = users_page.add_user_button_click()
    url = "http://host.docker.internal:8000/admin/auth/user/add/"

    with allure.step("Шаг 1: Переход на страницу создания пользователя."):
        with allure.step("Проверяем title"):
            assert add_user_page.get_title() == \
                   "Add user | Django site admin", \
                   "incorrect add user page title"

        with allure.step("Проверяем url"):
            assert add_user_page.get_url() == url, \
                "incorrect add user page url"

    user_name = MainSql.USER_NAME
    add_user_page.input_username(user_name)
    add_user_page.input_password()
    add_user_page.input_confirm_password()
    add_user_page.click_continue_button()

    with allure.step("Шаг 2: Создание пользователя. "
                     "Проверяем сообщение успешного создания пользователя"):
        assert add_user_page.get_add_user_message().text == \
           f"The user “{user_name}” was added successfully. " \
           f"You may edit it again below.", "absent add user message"

    group_id = MainSql.GROUP_ID
    add_user_page.user_add_in_group(group_id)

    db = database
    users = MainSql(cursor, db)
    user_id = users.get_user_id()
    group_user_id = users.get_user_id_from_group()

    with allure.step("Шаг 3: Добавление пользователя в группу. "
                     "Проверяем в БД что пользователь добавлен в группу"):
        assert user_id[0][0] == group_user_id[0][0], "user not in group"

    logout_page = admin_page.logout()

    with allure.step("Шаг 4: Выход из админки"):
        with allure.step("Проверяем title"):
            assert logout_page.get_title() == \
                   "Logged out | Django site admin", \
                   "incorrect logout page title"

        with allure.step("Проверяем url"):
            assert logout_page.get_url() == \
                   "http://host.docker.internal:8000/admin/logout/", \
                   "incorrect logout page url"

    users.delete_user_from_group()
    users.delete_user(user_id[0][0])
    users.delete_group()
