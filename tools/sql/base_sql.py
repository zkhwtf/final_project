class BaseSql:
    """
    Базовый класс для работы с sql.
    """

    def __init__(self, cursor, db):
        self.cursor = cursor
        self.db = db

    def select(self, select_arg: str, table_name: str, where_arg: str,
               where_value: str):
        """
        Метод select запроса.
        :param select_arg: значение выборки.
        :param table_name: имя таблицы.
        :param where_arg: аргумент условия.
        :param where_value: значение условия.
        :return: результат выборки.
        """
        query = f"SELECT {select_arg} FROM {table_name} " \
                f"WHERE {where_arg} = '{where_value}'"
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def insert(self, table_name, params, values) -> None:
        """
        Метод insert запроса.
        :param table_name: имя таблицы.
        :param params: имена столбцов.
        :param values: значения столбцов.
        :return: None.
        """
        query = f"INSERT INTO {table_name} ({params}) VALUES ({values})"
        self.cursor.execute(query)
        self.db.commit()

    def delete(self, table_name, params, values) -> None:
        """
        Метод delete запроса.
        :param table_name: имя таблицы.
        :param params: имя столбца.
        :param values: значение столбца.
        :return: None.
        """
        query = f"DELETE FROM {table_name} WHERE {params} = {values}"
        self.cursor.execute(query)
        self.db.commit()

    def delete_all(self, table_name) -> None:
        """
        Метод delete запроса без параметров.
        :param table_name: имя таблицы.
        :return: None.
        """
        self.cursor.execute(f"DELETE FROM {table_name}")
        self.db.commit()
