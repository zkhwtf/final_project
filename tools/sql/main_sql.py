from tools.sql.base_sql import BaseSql


class MainSql(BaseSql):
    """
    Класс для манипуляций с пользователями через sql.
    """
    USER_NAME = "test_user_26"
    PASSWORD = "pass123123"
    USERS_TABLE = "auth_user"
    USER_GROUP_TABLE = "auth_user_groups"

    GROUPS_TABLE = "auth_group"
    GROUP_NAME = "test_group_26"
    GROUP_ID = "26"

    def get_user_id(self) -> str:
        """
        Метод получения id пользователя.
        :return: id пользователя.
        """
        user_id = self.select("id", self.USERS_TABLE,
                              "username", self.USER_NAME)
        return user_id

    def get_user_id_from_group(self) -> str:
        """
        Метод получения id пользователя который состоит в группе.
        :return: имя пользователя.
        """
        user_group = self.select("user_id", self.USER_GROUP_TABLE,
                                 "group_id", self.GROUP_ID)
        return user_group

    def get_group_name(self) -> str:
        """
        Метод получения имени группы из БД.
        :return: имя группы в результате выборки.
        """
        group_name_db = self.select("name", self.GROUPS_TABLE,
                                    "name", self.GROUP_NAME)
        return group_name_db

    def add_new_group(self) -> None:
        """
        Метод создания группы.
        :return: None.
        """
        values = f"'{self.GROUP_ID}', '{self.GROUP_NAME}'"
        self.insert(self.GROUPS_TABLE, "id, name", values)

    def delete_group(self) -> None:
        """
        Метод удаления группы.
        :return: None.
        """
        self.delete(self.GROUPS_TABLE, "id", self.GROUP_ID)

    def delete_user(self, user_id) -> None:
        """
        Метод удаления пользователя.
        :param user_id: id пользователя.
        :return: None.
        """
        self.delete(self.USERS_TABLE, "id", user_id)

    def delete_user_from_group(self) -> None:
        """
        Метод удаления пользователя из группы.
        :return: None.
        """
        self.delete_all("auth_user_groups")
