from selenium.webdriver.common.by import By


class GroupsPageLocators:
    """
    Класс с локаторами groups page.
    """

    LOCATOR_GROUP = (By.XPATH, "//th[@class='field-__str__']/a")
