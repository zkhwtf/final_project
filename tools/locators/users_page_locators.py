from selenium.webdriver.common.by import By


class UsersPageLocators:
    """
    Класс с локаторами users page.
    """

    LOCATOR_ADD_USER_BUTTON = (By.XPATH, "//a[@href='/admin/auth/user/add/']")
    LOCATOR_USER = (By.XPATH, "//th[@class='field-username']/a")
