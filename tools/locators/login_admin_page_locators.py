from selenium.webdriver.common.by import By


class LoginAdminPageLocators:
    """
    Класс с локаторами и данными login admin page.
    """

    LOCATOR_USERNAME_FIELD = (By.XPATH, "//input[@name='username']")
    LOCATOR_PASSWORD_FIELD = (By.XPATH, "//input[@name='password']")
    LOCATOR_LOGIN_BUTTON = (By.XPATH, "//input[@type='submit']")

    USERNAME = "admin"
    PASSWORD = "password"
