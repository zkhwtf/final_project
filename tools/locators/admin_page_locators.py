from selenium.webdriver.common.by import By


class AdminPageLocators:
    """
    Класс с локаторами admin page.
    """

    LOCATOR_LOGOUT_BUTTON = (By.XPATH, "//a[@href='/admin/logout/']")
    LOCATOR_GROUPS_PAGE = (By.XPATH, "//a[@href='/admin/auth/group/']")
    LOCATOR_USERS_PAGE = (By.XPATH, "//a[@href='/admin/auth/user/']")
