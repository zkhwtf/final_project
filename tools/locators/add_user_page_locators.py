from selenium.webdriver.common.by import By


class AddUserPageLocators:
    """
    Класс с локаторами и данными add user page.
    """

    LOCATOR_USER_NAME_FIELD = (By.XPATH, "//input[@name='username']")
    LOCATOR_PASSWORD_FIELD = (By.XPATH, "//input[@name='password1']")
    LOCATOR_CONFIRM_PASSWORD_FIELD = (By.XPATH, "//input[@name='password2']")
    LOCATOR_SAVE_BUTTON = (By.XPATH, "//input[@value='Save']")
    LOCATOR_CONTINUE_BUTTON = (By.XPATH,
                               "//input[@value='Save and continue editing']")
    LOCATOR_ADD_USER_MESSAGE = (By.XPATH, "//li[@class='success']")
    LOCATOR_USER_ADD_IN_GROUP_BUTTON = (By.XPATH,
                                        "//a[@id='id_groups_add_all_link']")
    LOCATOR_FILTER = (By.XPATH, "//input[@id='id_groups_input']")
