from selenium.webdriver.common.by import By


class MainPageLocators:
    """
    Класс с локаторами и данными main page.
    """

    URL = "http://host.docker.internal:8000"
    LOCATOR_ADMIN_BUTTON = (By.XPATH, "//a[@href='/admin']")
