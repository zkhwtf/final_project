from tools.pages.admin_page import AdminPage
from tools.pages.base_page import BasePage
from tools.locators.login_admin_page_locators import LoginAdminPageLocators


class LoginAdminPage(BasePage):
    """
    Класс кстраницы admin login.
    """

    def get_title(self) -> str:
        """
        Метод получения заголовка окна.
        :return: заголовок окна.
        """
        return self.driver.title

    def get_url(self) -> str:
        """
        Метод получения url окна.
        :return: url окна.
        """
        return self.url

    def click_login_button(self):
        """
        Метод нажатия на кнопку login и переход на страницу admin.
        :return: страница admin.
        """
        login_button = self.clickable_element(LoginAdminPageLocators.
                                              LOCATOR_LOGIN_BUTTON)
        login_button.click()
        return AdminPage(self.driver, self.driver.current_url)

    def input_username_field(self) -> None:
        """
        Метод ввода username.
        :return: None.
        """
        username_field = self.clickable_element(LoginAdminPageLocators.
                                                LOCATOR_USERNAME_FIELD)
        username_field.send_keys(LoginAdminPageLocators.USERNAME)

    def input_password_field(self) -> None:
        """
        Метод ввода пароля.
        :return: None.
        """
        password_field = self.clickable_element(LoginAdminPageLocators.
                                                LOCATOR_PASSWORD_FIELD)
        password_field.send_keys(LoginAdminPageLocators.PASSWORD)
