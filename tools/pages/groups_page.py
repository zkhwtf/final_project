# from tools.pages.add_group_page import AddGroupPage
from tools.pages.base_page import BasePage
from tools.locators.groups_page_locators import GroupsPageLocators
# from tools.locators.add_group_page_locators import AddGroupPageLocators


class GroupsPage(BasePage):
    """
    Класс страницы groups.
    """

    def get_title(self) -> str:
        """
        Метод получения заголовка окна.
        :return: заголовок окна.
        """
        return self.driver.title

    def get_url(self) -> str:
        """
        Метод получения url окна.
        :return: url окна.
        """
        return self.url

    def get_group(self, group_name: str):
        """
        Метод получения элемента искомой группы.
        :param group_name: имя группы.
        :return: элемент искомой группы.
        """
        groups_list = self.find_elements(GroupsPageLocators.LOCATOR_GROUP)
        for group in groups_list:
            if group.text == group_name:
                return group
            else:
                continue
