from tools.pages.add_user_page import AddUserPage
from tools.pages.base_page import BasePage
from tools.locators.users_page_locators import UsersPageLocators


class UsersPage(BasePage):
    """
    Класс страницы users.
    """

    def get_title(self) -> str:
        """
        Метод получения заголовка окна.
        :return: заголовок окна.
        """
        return self.driver.title

    def get_url(self) -> str:
        """
        Метод получения url окна.
        :return: url окна.
        """
        return self.url

    def add_user_button_click(self):
        """
        Метод нажатия на кнопку add user и переход на страницу add user.
        :return: страница add user.
        """
        add_user_button = self.clickable_element(UsersPageLocators.
                                                 LOCATOR_ADD_USER_BUTTON)
        add_user_button.click()
        return AddUserPage(self.driver, self.driver.current_url)
