from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException


class BasePage:
    """
    Базовый класс страниц.
    """

    def __init__(self, driver, url):
        self.driver = driver
        self.url = url

    def open(self):
        """
        Метод открытия окна.
        """
        self.driver.get(self.url)

    def find_element(self, locator, timeout=5):
        """
        Метод поиска элемента.
        :param locator: локатор.
        :param timeout: время ожидания.
        :return: элемент если найден.
        """
        try:
            element = WebDriverWait(self.driver, timeout).until(
                EC.presence_of_element_located(locator))
            return element
        except TimeoutException:
            return None

    def clickable_element(self, locator, timeout=5):
        """
        Метод ожидания что элемент кликабелен.
        :param locator: локатор.
        :param timeout: время ожидания.
        :return: элемент если найден.
        """
        try:
            element = WebDriverWait(self.driver, timeout).until(
                EC.element_to_be_clickable(locator))
            return element
        except TimeoutException:
            return None

    def visible_element(self, locator, timeout=5):
        """
        Метод ожидания видимости элемента.
        :param locator: локатор.
        :param timeout: время ожидания.
        :return: элемент если найден.
        """
        try:
            element = WebDriverWait(self.driver, timeout).until(
                EC.visibility_of_element_located(locator))
            return element
        except TimeoutException:
            return None

    def find_elements(self, locator, timeout=5):
        """
        Метод поиска элементов.
        :param locator: локатор.
        :param timeout: время ожидания.
        :return: список элементов если найдены.
        """
        try:
            elements = WebDriverWait(self.driver, timeout).until(
                EC.presence_of_all_elements_located(locator))
            return elements
        except TimeoutException:
            return None
