from tools.pages.base_page import BasePage


class LogoutPage(BasePage):
    """
    Класс страницы logout.
    """

    def get_title(self) -> str:
        """
        Метод получения заголовка окна.
        :return: заголовок окна.
        """
        return self.driver.title

    def get_url(self) -> str:
        """
        Метод получения url окна.
        :return: url окна.
        """
        return self.url
