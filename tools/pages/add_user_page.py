from tools.pages.base_page import BasePage
from tools.locators.add_user_page_locators import AddUserPageLocators
from tools.sql.main_sql import MainSql


class AddUserPage(BasePage):
    """
    Класс страницы add user.
    """

    def get_title(self) -> str:
        """
        Метод получения заголовка окна.
        :return: заголовок окна.
        """
        return self.driver.title

    def get_url(self) -> str:
        """
        Метод получения url окна.
        :return: url окна.
        """
        return self.url

    def input_username(self, user_name: str) -> None:
        """
        Метод ввода username.
        :param user_name: имя пользователя.
        :return: None.
        """
        username_field = self.clickable_element(AddUserPageLocators.
                                                LOCATOR_USER_NAME_FIELD)
        username_field.send_keys(user_name)

    def input_password(self) -> None:
        """
        Метод ввода пароля.
        :return: None.
        """
        password_field = self.clickable_element(AddUserPageLocators.
                                                LOCATOR_PASSWORD_FIELD)
        password_field.send_keys(MainSql.PASSWORD)

    def input_confirm_password(self) -> None:
        """
        Метод ввода подтверждения пароля.
        :return: None.
        """
        confirm_password_field = self.clickable_element(
            AddUserPageLocators.LOCATOR_CONFIRM_PASSWORD_FIELD)
        confirm_password_field.send_keys(MainSql.PASSWORD)

    def click_continue_button(self) -> None:
        """
        Метод нажатия на кнопку continue.
        :return: None.
        """
        continue_button = self.clickable_element(AddUserPageLocators.
                                                 LOCATOR_CONTINUE_BUTTON)
        continue_button.click()

    def get_add_user_message(self):
        """
        Метод получения видимого элемента - сообщение о создании пользователя.
        :return: элемент сообщения.
        """
        add_user_message = self.visible_element(AddUserPageLocators.
                                                LOCATOR_ADD_USER_MESSAGE)
        return add_user_message

    def user_add_in_group(self, group_id: str) -> None:
        """
        Метод добавления пользователя в группу.
        :param group_id: id группы.
        :return: None.
        """

        filter_field = self.clickable_element(AddUserPageLocators.
                                              LOCATOR_FILTER)
        filter_field.send_keys(group_id)
        add_button = self.clickable_element(AddUserPageLocators.
                                            LOCATOR_USER_ADD_IN_GROUP_BUTTON)
        add_button.click()
        save_button = self.clickable_element(AddUserPageLocators.
                                             LOCATOR_SAVE_BUTTON)
        save_button.click()
