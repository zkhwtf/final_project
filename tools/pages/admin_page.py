from tools.pages.base_page import BasePage
from tools.pages.logout_page import LogoutPage
from tools.locators.admin_page_locators import AdminPageLocators
from tools.pages.groups_page import GroupsPage
from tools.pages.users_page import UsersPage


class AdminPage(BasePage):
    """
    Класс страницы admin.
    """

    def get_title(self) -> str:
        """
        Метод получения заголовка окна.
        :return: Заголовок окна.
        """
        return self.driver.title

    def get_url(self) -> str:
        """
        Метод получения url окна.
        :return: url окна.
        """
        return self.url

    def logout(self):
        """
        Метод нажатия на кнопку logout и перехода на страницу logout.
        :return: страница logout.
        """
        logout_button = self.clickable_element(AdminPageLocators.
                                               LOCATOR_LOGOUT_BUTTON)
        logout_button.click()
        return LogoutPage(self.driver, self.driver.current_url)

    def groups_page_click(self):
        """
        Метод нажатия на кнопку groups и перехода на страницу groups.
        :return: страница groups.
        """
        groups_page = self.visible_element(AdminPageLocators.
                                           LOCATOR_GROUPS_PAGE)
        groups_page.click()
        return GroupsPage(self.driver, self.driver.current_url)

    def users_page_click(self):
        """
        Метод нажатия на кнопку users и перехода на страницу users.
        :return: страница users.
        """
        users_page = self.visible_element(AdminPageLocators.LOCATOR_USERS_PAGE)
        users_page.click()
        return UsersPage(self.driver, self.driver.current_url)
