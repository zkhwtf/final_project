from tools.pages.base_page import BasePage
from tools.locators.main_page_locators import MainPageLocators
from tools.pages.login_admin_page import LoginAdminPage


class MainPage(BasePage):
    """
    Класс главной страницы.
    """

    URL = MainPageLocators.URL

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def get_title(self) -> str:
        """
        Метод получения заголовка окна.
        :return: заголовок окна.
        """
        return self.driver.title

    def get_url(self) -> str:
        """
        Метод получения url окна.
        :return: url окна.
        """
        return self.url

    def click_admin_button(self):
        """
        Метод нажатия на кнопку admin и переход на страницу login admin.
        :return: страница login admin.
        """
        admin_button = self.clickable_element(MainPageLocators.
                                              LOCATOR_ADMIN_BUTTON)
        admin_button.click()
        return LoginAdminPage(self.driver, self.driver.current_url)
