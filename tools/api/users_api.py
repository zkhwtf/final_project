from tools.api.base_api import BaseApi


class UserApi:
    """
    Класс api для манипуляций с пользователем.
    """

    USER_NAME = "test_user"
    PASSWORD = "123456789"

    BASE_URL = "https://petstore.swagger.io/v2"
    USER_PATH = "/user"
    LOGIN_PATH = "/user/login"
    LOGOUT_PATH = "/user/logout"

    LOGIN_HEADERS = {
        "username": USER_NAME,
        "password": PASSWORD
    }

    CREATE_USER_BODY = {
        "id": 543,
        "username": USER_NAME,
        "firstName": "Name",
        "lastName": "LastName",
        "email": "test_user@test.com",
        "password": PASSWORD,
        "phone": "0000-0000",
        "userStatus": 0
    }

    def __init__(self):
        self.client = BaseApi(self.BASE_URL)

    def create_user(self):
        """
        Метод создания пользователя.
        :return: ответ сервера.
        """
        return self.client.post(path=self.USER_PATH,
                                json=self.CREATE_USER_BODY)

    def login_user(self):
        """
        Метод логина пользователя.
        :return: ответ сервера.
        """
        return self.client.get(path=self.LOGIN_PATH,
                               headers=self.LOGIN_HEADERS)

    def logout_user(self):
        """
        Метод logout пользователя.
        :return: ответ сервера.
        """
        return self.client.get(path=self.LOGOUT_PATH)

    def delete_user(self):
        """
        Метод удаления пользователя.
        :return: ответ сервера.
        """
        return self.client.delete(path=self.USER_PATH,
                                  params=self.USER_NAME)

    def user_info(self):
        """
        Метод получения данных пользователя.
        :return: ответ сервера.
        """
        return self.client.get(path=self.USER_PATH,
                               params=self.USER_NAME)
