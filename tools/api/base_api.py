import requests


class BaseApi:
    """
    Базовый класс для работы с api.
    """

    def __init__(self, base_url):
        self.base_url = base_url

    def post(self, path=None, json=None):
        """
        Метод post запроса.
        :param path: часть запроса.
        :param json: json запроса.
        :return: ответ сервера.
        """
        url = f"{self.base_url}{path}"
        return requests.post(url=url, json=json)

    def get(self, path=None, headers=None, params=""):
        """
        Метод get запроса.
        :param path: часть запроса.
        :param headers: заголовок запроса.
        :param params: параметры запроса.
        :return: ответ сервера.
        """
        url = f"{self.base_url}{path}/{params}"
        return requests.get(url=url, headers=headers)

    def delete(self, path=None, params=None):
        """
        Метод delete запроса.
        :param path: часть запроса.
        :param params: параметры запроса.
        :return: ответ сервера.
        """
        url = f"{self.base_url}{path}/{params}"
        return requests.delete(url=url)
